{-# OPTIONS_HADDOCK prune #-} -- omitir definições que não têm documentação
{-|
Module      : Ficha2
Description : Módulo da resolução da Ficha 2 de Programação Funcional 2017-2018.
Copyright   : © Oracle, 2018
License     : GPL-3.0-or-later
Maintainer  : oracle.uminho@gmail.com
Tipos de dados e funções que compõe a resolução da Ficha 2 de Programação Funcional 2017-2018.
-}
module Ficha2
    (
    -- * Funções
      funA
    , funB
    , funC
    , funD
    , g
    , dobros
    , dobrosV2
    , numOcorre
    , numOcorreV2
    , positivos
    , positivosV2
    , soPos
    , soPosV2
    , somaNeg
    , somaNegV2
    , tresUlt
    , tresUltV2
    , soDigitos
    , soDigitosV2
    , minusculas
    , minusculasV2
    , nums
    , numsV2
    , segundos
    , segundosV2
    , nosPrimeiros
    , nosPrimeirosV2
    , nosPrimeirosV3
    , split
    , (><)
    , nosPrimeirosV4
    , minTuple
    , minTupleV2
    , minFst
    , minFstV2
    , sndMinFst
    , sndMinFstV2
    , sumTriplos
    , sumTriplosV2
    , maxTriplo
    , maxTriploV2
    )
where

import           Data.Char                                ( isDigit
                                                          , isLower
                                                          , digitToInt
                                                          )

-- 1

-- a
funA :: [Double] -> Double
funA []       = 0
funA (y : ys) = y ^ 2 + (funA ys)
{-
funA [2,3,5,1]
2 ^ 2 + funA [3,5,1]
2 ^ 2 + 3 ^ 2 + funA [5,1]
2 ^ 2 + 3 ^ 2 + 5 ^ 2 + funA [1]
2 ^ 2 + 3 ^ 2 + 5 ^ 2 + 1 ^ 2 + funA []
2 ^ 2 + 3 ^ 2 + 5 ^ 2 + 1 ^ 2 + 0
39.0
-}

-- b
funB :: [Int] -> [Int]
funB []      = []
funB (h : t) = if (mod h 2) == 0 then h : (funB t) else (funB t)
{-
funB [8,5,12]
(mod 8 2 == 0) ? True => 8 : funB [5,12]
8 : (mod 5 2 == 0) ? False => 8 : funB [12]
8 : (mod 12 2 == 0) ? True => 8 : 12 : funB []
8 : 12 : []
[8,12]
-}

-- c
funC (x : y : t) = funC t
funC [x        ] = []
funC []          = []
{-
funC [1,2,3,4,5]
funC [3,4,5]
funC [5]
[]
-}

-- d
funD l = g [] l
g l []      = l
g l (h : t) = g (h : l) t
{-
funD "otrec"
g [] "otrec"
g ('o' : []) "trec"
g ('t' : ('o' : [])) "rec"
g ('r' : ('t' : ('o' : []))) "ec"
g ('e' : ('r' : ('t' : ('o' : [])))) "c"
g ('c' : ('e' : ('r' : ('t' : ('o' : []))))) ""
'c' : ('e' : ('r' : ('t' : ('o' : []))))
"certo"
-}

-- 2

-- a
{-|
A função 'dobros' recebe uma lista de 'Float's e produz a lista em que cada elemento é o dobro do valor correspondente na lista de entrada.

== Exemplos de utilização:
>>> dobros []
[]

>>> dobros [1, 2, 3]
[2.0,4.0,6.0]
-}
dobros
    :: [Float] -- ^ A lista de entrada 
    -> [Float] -- ^ A lista com os dobros
dobros []      = []
dobros (h : t) = 2 * h : dobros t

dobrosV2 :: [Float] -> [Float]
dobrosV2 = map (* 2)

-- b
{-|
A função 'numOcorre' calcula o número de vezes que um caracter ocorre numa string.

== Exemplos de utilização:
>>> numOcorre 'a' ""
0

>>> numOcorre 'b' ['a', 'b', 'c', '1', '2', '3', ' ', 'b']
2

>>> numOcorre 'z' "abcdefghijklmnopqrstuvwxyz"
1
-}
numOcorre
    :: Char -- ^ O 'Char' dado
    -> String -- ^ A 'String' dada
    -> Int -- ^ O número de ocorrências
numOcorre _ [] = 0
numOcorre c (h : t) | c == h    = 1 + numOcorre c t
                    | otherwise = numOcorre c t

numOcorreV2 :: Char -> String -> Int
numOcorreV2 = (length .) . filter . (==)

-- c
{-|
A função 'positivos' verifica se se uma lista só tem elementos positivos.

== Exemplos de utilização:
>>> positivos []
False

>>> positivos [0]
False

>>> positivos [3]
True

>>> positivos [1, 2, 3]
True

>>> positivos [1, -2, 3]
False
-}
positivos
    :: [Int] -- ^ A lista dada
    -> Bool -- ^ O valor lógico da premissa
positivos []  = False
positivos [x] = x > 0
positivos (h : t) | h <= 0    = False
                  | otherwise = positivos t

positivosV2 :: [Int] -> Bool
positivosV2 = all (> 0)

-- d
{-|
A função 'soPos' retira todos os elementos não positivos de uma lista de inteiros.

== Exemplos de utilização:
>>> soPos []
[]

>>> soPos [0]
[]

>>> soPos [3]
[3]

>>> soPos [1, 2, 3]
[1,2,3]

>>> soPos [1, -2, 3]
[1,3]
-}
soPos
    :: [Int] -- ^ A lista dada
    -> [Int] -- ^ A lista de elementos não positivos
soPos [] = []
soPos (h : t) | h > 0     = h : soPos t
              | otherwise = soPos t

soPosV2 :: [Int] -> [Int]
soPosV2 = filter (> 0)

-- e
{-|
A função 'somaNeg' soma todos os números negativos da lista de entrada.

== Exemplos de utilização:
>>> somaNeg []
0

>>> somaNeg [-1]
-1

>>> somaNeg [1, -2]
-2

>>> somaNeg [1, -2, 3, -4, 5]
-6
-}
somaNeg
    :: [Int] -- ^ A lista dada
    -> Int  -- ^ O somatório dos números negativos
somaNeg [] = 0
somaNeg (h : t) | h < 0     = h + somaNeg t
                | otherwise = somaNeg t

somaNegV2 :: [Int] -> Int
somaNegV2 = sum . filter (< 0)

-- f
{-|
A função 'tresUlt' devolve os últimos três elementos de uma lista. Se a lista de entrada tiver menos de três elementos, devolve a própria lista.

== Exemplos de utilização:
>>> tresUlt []
[]

>>> tresUlt "a"
"a"

>>> tresUlt [-2.5, 3]
[-2.5,3.0]

>>> tresUlt ['a', 'b', 'c']
"abc"

>>> tresUlt [("Delete", -1), ("First", 0), ("Second", 1), ("Third", 2)]
[("First",0),("Second",1),("Third",2)]
-}
tresUlt
    :: [a] -- ^ A lista dada
    -> [a] -- ^ A lista dos (no máximo) 3 últimos elementos
tresUlt l | length l > 3 = (tresUlt . tail) l
          | otherwise    = l

tresUltV2 :: [a] -> [a]
tresUltV2 l = drop (length l - 3) l
-- 3

-- a
{-|
A função 'soDigitos' recebe uma lista de carateres, e selecciona dessa lista os caracteres que são algarismos.

== Exemplos de utilização:
>>> soDigitos []
""

>>> soDigitos "abc"
""

>>> soDigitos "1a2b3c"
"123"

>>> soDigitos ['z', '4', 'a', '2']
"42"
-}
soDigitos
    :: [Char] -- ^ A 'String' dada
    -> [Char] -- ^ A 'String' com os digítos 
soDigitos [] = []
soDigitos (h : t) | isDigit h = h : soDigitos t
                  | otherwise = soDigitos t

soDigitosV2 :: String -> String
soDigitosV2 = filter isDigit

-- b
{-|
A função 'minusculas' recebe uma lista de carateres, e conta quantos desses caracteres são letras minúsculas.

== Exemplos de utilização:
>>> minusculas ""
0

>>> minusculas "abc"
3

>>> minusculas ['A', '1', 'b', '2', 'C', '3']
1
-}
minusculas
    :: [Char] -- ^ A 'String' dado
    -> Int -- ^ O número de ocorrências
minusculas [] = 0
minusculas (h : t) | isLower h = 1 + minusculas t
                   | otherwise = minusculas t

minusculasV2 :: String -> Int
minusculasV2 = length . filter isLower

-- c
{-|
A função 'nums' recebe uma 'String' e devolve uma lista com os algarismos que occorem nessa 'String', pela mesma ordem.

== Exemplos de utilização:
>>> nums ""
[]

>>> nums "abc"
[]

>>> nums "0ah1Ç1do23yg5jsf8aaA"
[0,1,1,2,3,5,8]
-}
nums
    :: String -- ^ A 'String' dada 
    -> [Int] -- ^ A lista de inteiros correspondente
nums [] = []
nums (h : t) | isDigit h = digitToInt h : nums t
             | otherwise = nums t

numsV2 :: String -> [Int]
numsV2 = map digitToInt . filter isDigit

-- 4

-- a
{-|
A função 'segundos' calcula a lista das segundas componentes dos pares.

== Exemplos de utilização:
>>> segundos []
[]

>>> segundos [(1, 'a'), (2, 'b'), (3, 'c')]
"abc"

>>> segundos [("Beatriz", 33), ("Júlia", 44), ("Simão", 55)]
[33,44,55]
-}
segundos
    :: [(a, b)] -- ^ A lista de pares dada
    -> [b] -- ^ A lista dos segundos elementos
segundos []           = []
segundos ((_, b) : t) = b : segundos t

segundosV2 :: [(a, b)] -> [b]
segundosV2 = map snd

-- b
{-|
A função 'nosPrimeiros' verifica se um elemento aparece na lista como primeira componente de algum dos pares.

== Exemplos de utilização:
>>> nosPrimeiros 123 []
False

>>> nosPrimeiros 2 [(1, 'a'), (2, 'b'), (3, 'c')]
True

>>> nosPrimeiros "Joaquim" [("Beatriz", 33), ("Júlia", 44), ("Simão", 55)]
False
-}
nosPrimeiros
    :: (Eq a)
    => a -- ^ O elemento procurado
    -> [(a, b)] -- ^ A lista de pares
    -> Bool -- ^ O valor lógico da premissa
nosPrimeiros _ [] = False
nosPrimeiros x ((a, _) : t) | x == a    = True
                            | otherwise = nosPrimeiros x t

nosPrimeirosV2 :: (Eq a) => a -> [(a, b)] -> Bool
nosPrimeirosV2 = (. map fst) . elem

-- Made by (Not a Teacher) <3
nosPrimeirosV3 :: (Eq a) => a -> [(a, b)] -> Bool
nosPrimeirosV3 = flip (flip elem . map fst)

split :: (a -> b) -> (a -> c) -> a -> (b, c)
split f g x = (f x, g x)

(><) :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
f >< g = split (f . fst) (g . snd)

-- Made by (Not a Teacher) <3
nosPrimeirosV4 :: (Eq a) => a -> [(a, b)] -> Bool
nosPrimeirosV4 = curry ((uncurry elem) . (id >< map fst))

-- c
-- Função auxiliar que devolve o par duma lista cujo primeiro é o menor. Se a lista for fazia devolve um erro.
minTuple :: (Ord a) => [(a, b)] -> (a, b)
minTuple []  = errorWithoutStackTrace "Ficha2.minTuple: empty list"
minTuple [h] = h
minTuple (h : t) | fst h < a' = h
                 | otherwise  = (a', b')
    where (a', b') = minTuple t

minTupleV2 :: (Ord a) => [(a, b)] -> (a, b)
minTupleV2 = foldr1 (\x@(a, _) y@(c, _) -> if a < c then x else y)

{-|
A função 'minFst' calcula a menor primeira componente de uma lista de pares. Se a lista for fazia, devolve um erro.

== Exemplos de utilização:
>>> minFst []
*** Exception: Ficha2.minTuple: empty list

>>> minFst [(10, 21), (3, 55), (66, 3)]
3
-}
minFst
    :: (Ord a)
    => [(a, b)] -- ^ A lista de pares
    -> a -- ^ O menor primeiro elemento
minFst = fst . minTuple

minFstV2 :: (Ord a) => [(a, b)] -> a
minFstV2 = minimum . map fst

-- d
{-|
A função 'sndMinFst' calcula a segunda componente associada à menor primeira componente. Se a lista for fazia, devolve um erro.

== Exemplos de utilização:
>>> sndMinFst []
*** Exception: Ficha2.minTuple: empty list

>>> sndMinFst [(10, 21), (3, 55), (66, 3)]
55
-}
sndMinFst
    :: (Ord a)
    => [(a, b)] -- ^ A lista de pares
    -> b -- ^ O segundo elemento associado ao menor primeiro elemento
sndMinFst = snd . minTuple

sndMinFstV2 :: (Ord a) => [(a, b)] -> b
sndMinFstV2 = snd . minTupleV2

-- e
{-|
A função 'sumTriplos' soma uma lista de triplos componente a componente.

== Exemplos de utilização:
>>> sumTriplos []
(0,0,0)

>>> sumTriplos [(2, 4, 11), (3, 1, -5), (10, -3, 6)]
(15,2,12)
-}
sumTriplos
    :: (Num a, Num b, Num c)
    => [(a, b, c)] -- ^ A lista de triplos
    -> (a, b, c) -- ^ O triplo final
sumTriplos []              = (0, 0, 0)
sumTriplos ((a, b, c) : t) = (a + a', b + b', c + c')
    where (a', b', c') = sumTriplos t

sumTriplosV2 :: (Num a, Num b, Num c) => [(a, b, c)] -> (a, b, c)
sumTriplosV2 = foldr (\(a, b, c) (d, e, f) -> (a + d, b + e, c + f)) (0, 0, 0)

-- f
{-|
A função 'maxTriplo' calcula o máximo valor da soma das componentes de cada triplo de uma lista.

== Exemplos de utilização:
>>> maxTriplo []
(0,0,0)

>>> maxTriplo [(10, -4, 21), (3, 55, 20), (-8, 66, 4)]
78
-}
maxTriplo
    :: (Ord a, Num a)
    => [(a, a, a)] -- ^ A lista de triplos
    -> a -- ^ O máximo valor da soma interna de cada triplo
maxTriplo []              = 0
maxTriplo ((a, b, c) : t) = max (a + b + c) $ maxTriplo t

maxTriploV2 :: (Ord a, Num a) => [(a, a, a)] -> a
maxTriploV2 = maximum . map (\(a, b, c) -> a + b + c)
