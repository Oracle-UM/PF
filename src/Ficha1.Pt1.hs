{-# OPTIONS_HADDOCK prune #-} -- omitir definições que não têm documentação
{-|
Module      : Ficha1.Pt1
Description : Módulo da primeira parte da resolução da Ficha 1 de Programação Funcional 2017-2018.
Copyright   : © Oracle, 2018
License     : GPL-3.0-or-later
Maintainer  : oracle.uminho@gmail.com
Tipos de dados e funções que compõe a resolução da primeira parte da Ficha 1 de Programação Funcional 2017-2018 (Exercícios 1-3).
-}

module Ficha1.Pt1
    (
    -- * Tipos de dados
      Hora
    ,
    -- * Funções
      perimetro
    , dist
    , primUlt
    , multiplo
    , truncaImpar
    , max2
    , max3
    , nRaizes
    , raizes
    , isHourValid
    , isLatherThan
    , hora2min
    , min2hora
    , min2
    , diffHoras
    , addMin
    )
where

-- 1

-- a
{-|
A função 'perimetro' calcula o perímetro de uma circunferência, dado o comprimento do seu raio.
Se o raio for um número real positivo, devolve @Just (o períemtro)@, caso contrário devolve Nothing.

== Exemplos de utilização:
>>> perimetro 1
Just 6.283185307179586

>>> perimetro $ 1/2
Just 3.141592653589793

>>> perimetro $ -3
Nothing

>>> perimetro $ sqrt $ -2
Nothing
-}
perimetro
    :: (Ord a, Floating a)
    => a -- ^ O raio da circunferência (número real positivo) 
    -> Maybe a -- ^ O @Maybe@ perímetro da circunferência
perimetro r | r > 0     = Just $ 2 * pi * r
            | otherwise = Nothing

-- b
{-|
A função 'dist' calcula a distância entre dois pontos no plano Cartesiano. Cada ponto é um par de valores do tipo Double.

== Exemplos de utilização:
>>> dist (1, 0) (0, 0)
1.0

>>> dist (0, 5) (0, 10)
5.0

>>> dist (-1, -1) (1, 1)
2.8284271247461903
-}
dist
    :: (Double, Double) -- ^ As coordenadas reais do primeiro ponto
    -> (Double, Double) -- ^ As coordenadas reais do segundo ponto
    -> Double -- ^ A distância entre os dois pontos
dist (x1, y1) (x2, y2) = sqrt $ (x1 - x2) ** 2 + (y1 - y2) ** 2

-- c
{-|
A função 'primUlt' recebe uma lista e devolve um par com o primeiro e o último elemento dessa lista.

== Exemplos de utilização:
>>> primUlt []
(*** Exception: Prelude.head: empty list

>>> primUlt [1]
(1, 1)

>>> primUlt ["Ana", "Beatriz", "Joaquim", "Zeferino"]
("Ana", "Zeferino")

>>> primUlt [Just 1, Just 2, Nothing]
(Just 1, Nothing)
-}
primUlt
    :: [b] -- ^ A lista fornecida
    -> (b, b) -- ^ O par com o primeiro e o último elemento
primUlt l = (head l, last l)

-- d
{-|
A função 'multiplo' testa se o primeiro número inteiro é múltiplo do segundo número inteiro.

== Exemplos de utilização:
>>> multiplo 1 0
False

>>> multiplo 0 1
True

>>> multiplo 0 0
True

>>> multiplo 10 5
True

>>> multiplo (-1) 3
False
-}
multiplo
    :: Integral a
    => a -- ^ O (possível) múltiplo
    -> a -- ^ O (possível) divisor
    -> Bool -- ^ O valor lógico da premissa
multiplo m n | n == 0    = m == 0
             | otherwise = m `mod` n == 0

-- e
{-|
A função 'truncaImpar' recebe uma lista e, se o comprimento da lista for ímpar, retira-lhe o primeiro elemento, caso contrário devolve a própria lista.

== Exemplos de utilização:
>>> truncaImpar []
[]

>>> truncaImpar [1, 2, 3]
[2,3]

>>> truncaImpar ['a', 'b']
"ab"

>>> truncaImpar "abc"
"bc"

>>> truncaImpar [("Joaquim", 23), ("Roberto", 22), ("Isabel", 26)]
[("Roberto",22),("Isabel",26)]
-}
truncaImpar
    :: [a] -- ^ A lista fornecida
    -> [a] -- ^ A lista final
truncaImpar l | length l `mod` 2 /= 0 = tail l
              | otherwise             = l

-- f
{-|
A função 'max2' calcula o maior de dois números inteiros.

== Exemplos de utilização:
>>> max2 0 0
0

>>> max2 2 1
2

>>> max2 (-2) $ -1
-1
-}
max2
    :: Integral a
    => a -- ^ O primeiro inteiro 
    -> a -- ^ O segundo inteiro
    -> a -- ^ O maior inteiro
max2 x y = if x > y then x else y

-- g
{-|
A função 'max3' calcula o maior de três números inteiros.

== Exemplos de utilização:
>>> max3 0 0 0
0

>>> max3 0 3 $ -2
3

>>> max3 22 20 $ -1
22
-}
max3
    :: Integral a
    => a -- ^ O primeiro inteiro 
    -> a -- ^ O segundo inteiro
    -> a -- ^ O terceiro inteiro
    -> a -- ^ O maior inteiro
max3 x y z = max2 x $ max2 y z

-- 2

-- a
{-|
A função 'nRaizes' recebe os 3 primeiros coeficientes de um polinómio de 2º grau e calcula o número de raízes reais desse polinómio.

__Nota:__ Como os argumentos são os coeficientes de um polinómio de 2º grau, o primeiro coeficiente tem de ser obrigatoriamente diferente de 0.

== Exemplos de utilização:
>>> nRaizes 1 0 5
0

>>> nRaizes (-3.2) 8 $ -5
1

>>> nRaizes 2.7 pi $ -33
2
-}
nRaizes
    :: (Ord a, Floating a)
    => a -- ^ O coeficiente da variável de grau 2
    -> a -- ^ O coeficiente da variável de grau 1
    -> a -- ^ O coeficiente da variável de grau 0
    -> Int -- ^ O número de raízes reais
nRaizes a b c | discriminant > 0  = 2
              | discriminant == 0 = 1
              | otherwise         = 0
    where discriminant = b ** 2 - 4 * a * c

-- b
{-|
A função 'raizes' recebe os 3 primeiros coeficientes de um polinómio de 2º grau e calcula a lista das suas raízes reais.

__Nota:__ Como os argumentos são os coeficientes de uma função de 2º grau, o primeiro coeficiente tem de ser obrigatoriamente diferente de 0.

== Exemplos de utilização:
>>> raizes 1 0 3
[]

>>> raizes 3 (-2) $ 1/3
[0.3333333333333333]

>>> raizes 1 0 $ -pi ** 2
[3.141592653589793,-3.141592653589793]
-}
raizes
    :: (Ord a, Floating a)
    => a -- ^ O coeficiente da variável de grau 2
    -> a -- ^ O coeficiente da variável de grau 1
    -> a -- ^ O coeficiente da variável de grau 0
    -> [a] -- ^ A lista com as raízes reais do polinómio
raizes a b c | nRaizes a b c == 2 = [x1, x2]
             | nRaizes a b c == 1 = [x1]
             | otherwise          = []
  where
    x1           = e + sqrt discriminant / (2 * a)
    x2           = e - sqrt discriminant / (2 * a)
    discriminant = b ** 2 - 4 * a * c
    e            = -b / (2 * a)

-- 3
-- | Representação de horas por um par de números inteiros, em que o primeiro elemento é o número de horas e o segundo elemento é o número de minutos.
type Hora = (Int, Int)

-- a
{-|
A função 'isHourValid' verifica se a hora dada é válida i.e., se o número de horas está entre 0 e 23 e o número de minutos entre 0 e 59 (inclusive). 

== Exemplos de utilização:
>>> isHourValid (-3, 27)
False

>>> isHourValid (10, 61)
False

>>> isHourValid (24, 0)
False

>>> isHourValid (0, 0)
True

>>> isHourValid (12, 60)
False

>>> isHourValid (12, 30)
True
-}
isHourValid
    :: Hora -- ^ A 'Hora' dada
    -> Bool -- ^ O valor lógico da premissa
isHourValid (h, m) = h >= 0 && h < 24 && m >= 0 && m < 60

-- b
{-|
A função 'isLatherThan' verifica se a primeira 'Hora' ocorre ou não depois da segunda 'Hora'.

== Exemplos de utilização:
>>> isLatherThan (1, 1) (1, 0)
True

>>> isLatherThan (2, 3) (3, 2)
False

>>> isLatherThan (0, 0) (23, 59)
False
-}
isLatherThan
    :: Hora -- ^ A primeira 'Hora'
    -> Hora -- ^ A segunda 'Hora'
    -> Bool -- ^ O valor lógico da premissa
isLatherThan (h1, m1) (h2, m2) = h1 > h2 || h1 == h2 && m1 > m2

-- c
{-|
A função 'hora2min' converte uma 'Hora' no valor correspondente em minutos.

== Exemplos de utilização:
>>> hora2min (0, 0)
0

>>> hora2min (1, 0)
60

>>> hora2min (0, 33)
33

>>> hora2min (24, 0)
1440

>>> hora2min (17, 28)
1048
-}
hora2min
    :: Hora -- ^ A 'Hora' dada
    -> Int -- ^ O seu valor em minutos
hora2min (h, m) = h * 60 + m

-- d
{-|
A função 'min2hora' converte um valor em minutos na 'Hora' (válida -- ver 'isHourValid') correspondente.

== Exemplos de utilização:
>>> min2hora 0
(0,0)

>>> min2hora 23
(0,23)

>>> min2hora 60
(1,0)

>>> min2hora $ 24 * 60 + 1
(0, 1)

>>> (min2hora . hora2min) (23, 18)
(23,18)
-}
min2hora
    :: Int  -- ^ O valor em minutos
    -> Hora -- ^ A 'Hora' correspondente
min2hora m = (h', m')
  where
    h' = (m `div` 60) `mod` 24
    m' = m `mod` 60

-- e
-- Função auxiliar que cálcula o mínimo entre 2 números reais.
min2 :: Ord p => p -> p -> p
min2 x y = if x < y then x else y

{-|
A função 'diffHoras' calcula a diferença (em minutos) entre duas 'Hora's.

__Nota:__ É levada em conta a possibilidade de horas pertencerem a dias diferentes, e.g. a diferença entre as 23:00 e a 01:00 ser 120 minutos, e não 1320 minutos.

== Exemplos de utilização:
>>> diffHoras (10, 0) (10, 20)
20

>>> diffHoras (23, 59) (0, 0)
1

>>> diffHoras (12, 0) (12, 0)
0

>>> diffHoras (12, 0) (13, 0)
60

>>> diffHoras (12, 1) (0, 0)
719

>>> diffHoras (11, 59) (0, 0)
719
-}
diffHoras
    :: Hora -- ^ A primeira 'Hora'
    -> Hora -- ^ A segunda 'Hora'
    -> Int -- ^ A diferença em minutos
diffHoras t1 t2 = min2 delta_1 delta_2
  where
    delta_1 = abs $ hora2min t1 - hora2min t2
    delta_2 = hora2min (24, 0) - delta_1

-- f
{-|
A função 'addMin' adiciona um determinado número de minutos a uma dada 'Hora', retornando o resultado em 'Hora's (válidas -- ver 'isHourValid').

== Exemplos de utilização:
>>> addMin (0, 3) 0
(0,3)

>>> addMin (0, 3) 10
(0,13)

>>> addMin (23, 0) 61
(0,1)

>>> addMin (12, 30) 98
(14,8)

>>> addMin (0, 0) $ 24 * 60
(0,0)
-}
addMin
    :: Hora -- ^ A 'Hora' inicial
    -> Int -- ^ O número de minutos incrementados
    -> Hora -- ^ A 'Hora' final
addMin h m = min2hora (hora2min h + m)
