{-# OPTIONS_HADDOCK prune #-} -- omitir definições que não têm documentação
{-|
Module      : Ficha3
Description : Módulo da resolução da Ficha 3 de Programação Funcional 2017-2018.
Copyright   : © Oracle, 2018
License     : GPL-3.0-or-later
Maintainer  : oracle.uminho@gmail.com
Tipos de dados e funções que compõe a resolução da Ficha 3 de Programação Funcional 2017-2018.
-}
module Ficha3
    (
    -- * Funções  
    )
where

