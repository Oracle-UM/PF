{-# OPTIONS_HADDOCK prune #-} -- omitir definições que não têm documentação
{-|
Module      : Ficha1.Pt2
Description : Módulo da segunda parte da resolução da Ficha 1 de Programação Funcional 2017-2018.
Copyright   : © Oracle, 2018
License     : GPL-3.0-or-later
Maintainer  : oracle.uminho@gmail.com
Tipos de dados e funções que compõe a resolução da segunda parte da Ficha 1 de Programação Funcional 2017-2018 (Exercícios 4-8).
-}

module Ficha1.Pt2
    (
    -- * Tipos de dados
      Hora(..)
    , Semaforo(..)
    , Ponto(..)
    , Figura(..)
    ,
    -- * Funções
      isHourValid
    , isLatherThan
    , hora2min
    , min2hora
    , diffHoras
    , addMin
    , next
    , stop
    , safe
    , posx
    , posy
    , raio
    , normalizeAng
    , angulo
    , pol2rect
    , dist
    , poligono
    , vertices
    , getSideLengths
    , area
    , perimetro
    , isLower
    , isDigit
    , isAlpha
    , toUpper
    , intToDigit
    , digitToInt
    )
where

import           Ficha1.Pt1                               ( min2 )
import           Data.Char                                ( ord
                                                          , chr
                                                          )

-- 4

-- | Representação de horas por um construtor 'H' seguido de dois números inteiros, em que o primeiro número inteiro é o número de horas e o segundo número inteiro é o número de minutos.
data Hora = H Int Int deriving (Show, Eq)

-- a
{-|
A função 'isHourValid' verifica se a hora dada é válida i.e., se o número de horas está entre 0 e 23 e o número de minutos entre 0 e 59 (inclusive). 

== Exemplos de utilização:
>>> isHourValid $ H (-3) 27
False

>>> isHourValid $ H 10 61
False

>>> isHourValid $ H 24 0
False

>>> isHourValid $ H 0 0
True

>>> isHourValid $ H 12 60
False

>>> isHourValid $ H 12 30
True
-}
isHourValid
    :: Hora -- ^ A 'Hora' dada
    -> Bool -- ^ O valor lógico da premissa
isHourValid (H h m) = h >= 0 && h < 24 && m >= 0 && m < 60

-- b
{-|
A função 'isLatherThan' verifica se a primeira 'Hora' ocorre ou não depois da segunda 'Hora'.

== Exemplos de utilização:
>>> isLatherThan (H 1 1) $ H 1 0
True

>>> isLatherThan (H 2 3) $ H 3 2
False

>>> isLatherThan (H 0 0) $ H 23 59
False
-}
isLatherThan
    :: Hora -- ^ A primeira 'Hora'
    -> Hora -- ^ A segunda 'Hora'
    -> Bool -- ^ O valor lógico da premissa
isLatherThan (H h1 m1) (H h2 m2) = h1 > h2 || h1 == h2 && m1 > m2

-- c
{-|
A função 'hora2min' converte uma 'Hora' no valor correspondente em minutos.

== Exemplos de utilização:
>>> hora2min $ H 0 0
0

>>> hora2min $ H 1 0
60

>>> hora2min $ H 0 33
33

>>> hora2min $ H 24 0
1440

>>> hora2min $ H 17 28
1048
-}
hora2min
    :: Hora -- ^ A 'Hora' dada
    -> Int -- ^ O seu valor em minutos
hora2min (H h m) = h * 60 + m

-- d
{-|
A função 'min2hora' converte um valor em minutos na 'Hora' (válida -- ver 'isHourValid') correspondente.

== Exemplos de utilização:
>>> min2hora 0
H 0 0

>>> min2hora 23
H 0 23

>>> min2hora 60
H 1 0

>>> min2hora $ 24 * 60 + 1
H 0 1

>>> (min2hora . hora2min) $ H 23 18
H 23 18
-}
min2hora
    :: Int -- ^ O valor em minutos
    -> Hora -- ^ A 'Hora' correspondente
min2hora m = H h' m'
  where
    h' = (m `div` 60) `mod` 24
    m' = m `mod` 60

-- e
{-|
A função 'diffHoras' calcula a diferença (em minutos) entre duas 'Hora's.

__Nota:__ É levada em conta a possibilidade de horas pertencerem a dias diferentes, e.g. a diferença entre as 23:00 e a 01:00 ser 120 minutos, e não 1320 minutos.

== Exemplos de utilização:
>>> diffHoras (H 10 0) $ H 10 20
20

>>> diffHoras (H 23 59) $ H 0 0
1

>>> diffHoras (H 12 0) $ H 12 0
0

>>> diffHoras (H 12 0) $ H 13 0
60

>>> diffHoras (H 12 1) $ H 0 0
719

>>> diffHoras (H 11 59) $ H 0 0
719
-}
diffHoras
    :: Hora -- ^ A primeira 'Hora'
    -> Hora -- ^ A segunda 'Hora'
    -> Int -- ^ A diferença em minutos
diffHoras t1 t2 = min2 delta_1 delta_2
  where
    delta_1 = abs $ hora2min t1 - hora2min t2
    delta_2 = hora2min (H 24 60) - delta_1

-- f
{-|
A função 'addMin' adiciona um determinado número de minutos a uma dada 'Hora', retornando o resultado em 'Hora's (válidas -- ver 'isHourValid').

== Exemplos de utilização:
>>> addMin (H 0 3) 0
H 0 3

>>> addMin (H 0 3) 10
H 0 13

>>> addMin (H 23 0) 61
H 0 1

>>> addMin (H 12 30) 98
H 14 8

>>> addMin (H 0 0) $ 24 * 60
H 0 0
-}
addMin
    :: Hora -- ^ A 'Hora' inicial
    -> Int -- ^ O número de minutos incrementados
    -> Hora -- ^ A 'Hora' final
addMin h m = min2hora (hora2min h + m)

-- 5

-- | Representação dos possı́veis estados de um semáforo.
data Semaforo = Verde | Amarelo | Vermelho deriving (Show, Eq)

-- a
{-|
A função 'next' calcula o próximo estado de um 'Semaforo'.

== Exemplos de utilização:
>>> next Verde
Amarelo

>>> next Amarelo
Vermelho

>>> next Vermelho
Verde
-}
next
    :: Semaforo -- ^ O 'Semaforo' dado
    -> Semaforo -- ^ O 'Semaforo' seguinte
next s = case s of
    Verde    -> Amarelo
    Amarelo  -> Vermelho
    Vermelho -> Verde

-- b
{-|
A função 'stop' determina se é obrigatório parar ou não num 'Semaforo'.

__Nota:__ É assumido que é obrigatório parar quando o 'Semaforo' está a 'Amarelo' ou a 'Vermelho' (i.e., diferente de 'Verde').

== Exemplos de utilização:
>>> stop Verde
False

>>> stop Amarelo
True

>>> stop Vermelho
True
-}
stop
    :: Semaforo -- ^ O 'Semaforo' dado
    -> Bool -- ^ O valor lógico da premissa
stop = (/=) Verde

-- c
{-|
A função 'safe' testa se o estado de dois 'Semaforo's num cruzamento é seguro.

__Nota:__ É assumido que um cruzamento é seguro quando não o é possível atravessar em duas direções simultaneamente, i.e., quando pelo menos um dos 'Semaforo's é 'Vermelho'.

== Exemplos de utilização:
>>> safe Verde Verde
False

>>> safe Amarelo Verde
False

>>> safe Vermelho Verde
True

>>> safe Vermelho Amarelo
True
-}
safe
    :: Semaforo -- ^ O primeiro 'Semaforo' 
    -> Semaforo -- ^ O segundo 'Semaforo'
    -> Bool -- ^ O valor lógico da premissa
safe s1 s2 = s1 == Vermelho || s2 == Vermelho

-- 6
-- | Representação de um ponto no plano por um sistema de coordenadas.
data Ponto
    = Cartesiano Double Double -- ^ Coordendas cartesianas (distâncias aos eixos vertical e horizontal)
    | Polar Double Double -- ^ Coordendas polares (distância à origem e ângulo do respectivo vetor com o eixo horizontal
    deriving (Show, Eq)

-- a
{-|
A função 'posx' calcula a distância de um 'Ponto' ao eixo vertical.

__Nota:__ É assumido que o raio nas coordenadas polares é maior ou igual a 0.

== Exemplos de utilização:
>>> posx $ Cartesiano 0 1
0.0

>>> posx $ Cartesiano (-2.3) 1.37
2.3

>>> posx $ Polar 0 pi
0.0

>>> posx $ Polar (sqrt 2) $ 3 * pi/4
1.0
-}
posx
    :: Ponto -- ^ O 'Ponto' dado
    -> Double -- ^ A distância ao eixo das ordenadas
posx (Cartesiano x _  ) = abs x
posx (Polar      r ang) = abs $ r * cos ang

-- b
{-|
A função 'posy' calcula a distância de um 'Ponto' ao eixo horizontal.

__Nota:__ É assumido que o raio nas coordenadas polares é maior ou igual a 0.

== Exemplos de utilização:
>>> posy $ Cartesiano 1 0
0.0

>>> posy $ Cartesiano (-22.3) 12.223
12.223

>>> posy $ Polar 0 pi
0.0

>>> posy $ Polar 1 $ -pi / 6
0.49999999999999994
-}
posy
    :: Ponto -- ^ O 'Ponto' dado
    -> Double -- ^ A distância ao eixo das abcissas
posy (Cartesiano _ y  ) = abs y
posy (Polar      r ang) = abs $ r * sin ang

-- c
{-|
A função 'raio' calcula a distância de um 'Ponto' à origem.

__Nota:__ É assumido que o raio nas coordenadas polares é maior ou igual a 0.

== Exemplos de utilização:
>>> raio $ Cartesiano 1 1
1.4142135623730951

>>> raio $ Cartesiano (-3) $ sqrt 7
4.0

>>> raio $ Polar 2 pi
2.0
-}
raio
    :: Ponto -- ^ O 'Ponto' dado
    -> Double -- ^ A distância à origem
raio p = dist p $ Cartesiano 0 0

-- d
-- Função auxiliar que normaliza um ângulo em radianos ao intervalo [-pi, pi].
normalizeAng :: Double -> Double
normalizeAng ang | ang > pi  = normalizeAng (ang - 2 * pi)
                 | ang < -pi = normalizeAng (ang + 2 * pi)
                 | otherwise = ang

{-|
A função 'angulo' calcula o ângulo (em radianos) entre o vetor que liga a origem ao 'Ponto' dado e o eixo horizontal.

__Nota:__ O ângulo é normalizado ao intervalo @[-pi, pi]@.

== Exemplos de utilização:
>>> angulo $ Cartesiano 0 0
*** Exception: Prelude.undefined
CallStack (from HasCallStack):
  error, called at libraries/base/GHC/Err.hs:79:14 in base:GHC.Err
  undefined, called at Pt2.hs:406:45 in main:Ficha1.Pt2

>>> angulo $ Cartesiano 1 0
0.0

>>> angulo $ Cartesiano 0 2.4
1.5707963267948966

>>> angulo $ Cartesiano (-1) 0
3.141592653589793

>>> angulo $ Polar 1 $ 2 * pi
0.0

>>> angulo $ Polar 1 $ 3 * pi
3.141592653589793

>>> angulo $ Polar 2.5 $ pi / 2
1.5707963267948966
-}
angulo
    :: Ponto -- ^ O 'Ponto' dado
    -> Double -- ^ O ângulo correspondente
angulo (Cartesiano x y) | x > 0           = atan (y / x)
                        | x < 0 && y >= 0 = atan (y / x) + pi
                        | x < 0 && y < 0  = atan (y / x) - pi
                        | x == 0 && y > 0 = pi / 2
                        | x == 0 && y < 0 = -pi / 2
                        | otherwise       = undefined
angulo (Polar _ ang) = normalizeAng ang


-- e
-- Função auxiliar que converte um 'Ponto' para coordenadas cartesianas.
pol2rect :: Ponto -> Ponto
pol2rect (Polar r ang) = Cartesiano (r * cos ang) (r * sin ang)
pol2rect p             = p

{-|
A função 'dist' calcula a distância entre dois 'Ponto's.

== Exemplos de utilização:
>>> dist (Cartesiano 0 0) (Cartesiano 0 5)
5.0

>>> dist (Cartesiano 3.1 2.8) (Cartesiano 2.8 3.1)
0.4242640687119289

>>> dist (Polar 2 pi) (Polar 3 pi)
1.0

>>> dist (Polar 1 $ pi / 4) (Polar 2 $ 3 * pi / 4)
2.2360679774997894

>>> dist (Polar 3 pi) (Cartesiano (-3) 20)
20.0
-}
dist
    :: Ponto -- ^ O primeiro 'Ponto'
    -> Ponto -- ^ O segundo 'Ponto'
    -> Double -- ^ A distância entre os dois 'Ponto's
dist p1 p2 = sqrt ((x1 - x2) ** 2 + (y1 - y2) ** 2)
  where
    Cartesiano x1 y1 = pol2rect p1
    Cartesiano x2 y2 = pol2rect p2

-- 7 
-- | Representação de uma figura geométrica no plano.
data Figura = Circulo Ponto Double -- ^ Um círculo centrado num 'Ponto' com um raio 'Double'
            | Retangulo Ponto Ponto -- ^ Um retângulo paralelo aos eixos representado por dois 'Ponto's que são vértices da sua diagonal
            | Triangulo Ponto Ponto Ponto -- ^ Um triângulo representado pelos três 'Ponto's dos seus vértices
              deriving (Show, Eq)

-- a
{-|
A função 'poligono' verifica se uma 'Figura' é um polígono, i.e., uma figura com lados, fechada.

== Exemplos de utilização:
>>> poligono $ Circulo (Cartesiano 0 0) 1
False

>>> poligono $ Retangulo (Cartesiano 1 1) (Cartesiano 0 0)
True

>>> poligono $ Triangulo (Polar 0.5 pi) (Polar 0.5 0) (Polar 1 $ pi/2)
True
-}
poligono
    :: Figura -- ^ A 'Figura' dada 
    -> Bool -- ^ O valor lógico da premissa
poligono Circulo{} = False
poligono _         = True

-- b
{-|
A função 'vertices' calcula a lista dos vértices de uma 'Figura'.

__Nota:__ Os vértices retornados são representados por coordenadas cartesianas.

== Exemplos de utilização:
>>> vertices $ Circulo (Cartesiano 0 0) 1
[]

>>> vertices $ Retangulo (Cartesiano 1 1) (Cartesiano 0 0)
[Cartesiano 1.0 1.0,Cartesiano 0.0 1.0,Cartesiano 1.0 0.0,Cartesiano 0.0 0.0]

>>> vertices $ Triangulo (Polar 0.5 pi) (Polar 0.5 0) (Polar 1 $ pi/2)
[Cartesiano (-0.5) 6.123233995736766e-17,Cartesiano 0.5 0.0,Cartesiano 6.123233995736766e-17 1.0]
-}
vertices
    :: Figura -- ^ A 'Figura' dada
    -> [Ponto] -- ^ A lista de vértices
vertices Circulo{}         = []
vertices (Retangulo p1 p4) = [p1', p2, p3, p4']
  where
    p1'@(Cartesiano x1 y1) = pol2rect p1
    p4'@(Cartesiano x4 y4) = pol2rect p4
    p2                     = Cartesiano x4 y1
    p3                     = Cartesiano x1 y4
vertices (Triangulo v1 v2 v3) = [pol2rect v1, pol2rect v2, pol2rect v3]

-- c
-- Função auxiliar que devolve a lista com as dimensões dos lados da 'Figura' dada.
getSideLengths :: Figura -> [Double]
getSideLengths Circulo{}         = []
getSideLengths (Retangulo p1 p4) = [c, l]
  where
    Cartesiano x1 y1 = pol2rect p1
    Cartesiano x4 y4 = pol2rect p4
    c                = abs $ x1 - x4
    l                = abs $ y1 - y4
getSideLengths (Triangulo p1 p2 p3) = [l1, l2, l3]
  where
    l1 = dist p1 p2
    l2 = dist p2 p3
    l3 = dist p3 p1

{-|
A função 'area' calcula a área de uma 'Figura'.

== Exemplos de utilização:
>>> area $ Circulo (Cartesiano 0 0) 1
3.141592653589793

>>> area $ Retangulo (Cartesiano 1 1) (Cartesiano 6 2)
5.0

>>> area $ Triangulo (Cartesiano (-0.5) 0) (Cartesiano 0.5 0) (Cartesiano 0 1)
0.5
-}
area
    :: Figura -- ^ A 'Figura' dada
    -> Double -- ^ A área
area (Circulo _ r) = pi * r ** 2
area r@Retangulo{} = (product . getSideLengths) r
area t@Triangulo{} = sqrt (s * (s - l1) * (s - l2) * (s - l3)) -- formula de Heron
  where
    side_lengths = getSideLengths t
    l1           = side_lengths !! 0
    l2           = side_lengths !! 1
    l3           = side_lengths !! 2
    s            = (l1 + l2 + l3) / 2 -- semi-perimetro

-- d
{-|
A função 'perimetro' calcula o perímetro de uma 'Figura'.

== Exemplos de utilização:
>>> perimetro $ Circulo (Cartesiano 0 0) $ 1 / (2 * pi)
1.0

>>> perimetro $ Retangulo (Cartesiano 1 1) (Cartesiano 6 2)
12.0

>>> perimetro $ Triangulo (Cartesiano 0 0) (Cartesiano 1 0) (Cartesiano 0 1)
3.414213562373095
-}
perimetro
    :: Figura -- ^ A 'Figura' dada
    -> Double -- ^ O perímetro
perimetro (Circulo _ r) = 2 * pi * r
perimetro r@Retangulo{} = 2 * (sum . getSideLengths) r
perimetro t@Triangulo{} = (sum . getSideLengths) t

-- 8

-- a
{-|
A função 'isLower' verifica se o 'Char' dado é uma letra minúscula.

__Nota:__ Ao contrário da função predefinida no <http://hackage.haskell.org/package/base-4.11.1.0/docs/Data-Char.html Data.Char>, não leva em conta carateres especiais, e.g., \'ç\', \'ã\', \'ó\', apenas carateres de \'a\' a \'z\'.

== Exemplos de utilização:
>>> isLower 'a'
True

>>> isLower 'A'
False
-}
isLower
    :: Char -- ^ O 'Char' dado
    -> Bool -- ^ O valor lógico da premissa
isLower c = c >= 'a' && c <= 'z'

-- b
{-|
A função 'isDigit' verifica se o 'Char' dado é um dígito.

== Exemplos de utilização:
>>> isDigit '1'
True

>>> isDigit 'G'
False
-}
isDigit
    :: Char -- ^ O 'Char' dado
    -> Bool -- ^ O valor lógico da premissa
isDigit c = c >= '0' && c <= '9'

-- c
{-|
A função 'isAlpha' verifica se o 'Char' dado é uma letra.

__Nota:__ Ao contrário da função predefinida no <http://hackage.haskell.org/package/base-4.11.1.0/docs/Data-Char.html Data.Char>, não leva em conta carateres especiais, e.g., \'Ç\', \'ã\', \'Ó\', apenas carateres de \'a\' a \'z\' e de \'A\' a \'Z\'.

== Exemplos de utilização:
>>> isAlpha 'A'
True

>>> isAlpha 'b'
True

>>> isAlpha '3'
False
-}
isAlpha
    :: Char -- ^ O 'Char' dado
    -> Bool -- ^ O valor lógico da premissa
isAlpha c = isLower c || c >= 'A' && c <= 'Z'

-- d
{-|
A função 'toUpper' converte uma letra na letra maiúscula correspondente.

__Nota:__ Ao contrário da função predefinida no <http://hackage.haskell.org/package/base-4.11.1.0/docs/Data-Char.html Data.Char>, não leva em conta carateres especiais, e.g., \'Ç\', \'ã\', \'Ó\', apenas carateres de \'a\' a \'z\' e de \'A\' a \'Z\'.

== Exemplos de utilização:
>>> toUpper 'a'
'A'

>>> toUpper 'B'
'B'
-}
toUpper
    :: Char -- ^ A letra dada
    -> Char -- ^ A letra maiúscula correspondente
toUpper c | isLower c = chr $ ord c + diff
          | otherwise = c
    where diff = ord 'A' - ord 'a'

-- e
{-|
A função 'intToDigit' converte um número entre 0 e 9 no respetivo dígito.

== Exemplos de utilização:
>>> intToDigit 2
'2'

>>> intToDigit 23
*** Exception: Char.intToDigit: not a digit 23
-}
intToDigit
    :: Int -- ^ O número dado
    -> Char -- ^ O dígito correspondente
intToDigit i
    | i >= 0 && i <= 9 = chr (i + ord '0')
    | otherwise = errorWithoutStackTrace
        ("Ficha1.Pt1.intToDigit: not a digit " ++ show i)

-- f
{-|
A função 'digitToInt' converte um dígito no respetivo inteiro.

__Nota:__ Ao contrário da função predefinida no <http://hackage.haskell.org/package/base-4.11.1.0/docs/Data-Char.html Data.Char>, não leva em conta dígitos hexadecimais, e.g., \'a\', \'B\', \'f\', apenas dígitos de \'0\' a \'9\'.

== Exemplos de utilização:
>>> digitToInt '0'
0

>>> digitToInt 'a'
*** Exception: Char.digitToInt: not a digit 'a'
-}
digitToInt
    :: Char -- ^ O dígito dado 
    -> Int -- ^ O inteiro correspondente
digitToInt c
    | isDigit c = ord c - ord '0'
    | otherwise = errorWithoutStackTrace
        ("Ficha1.Pt1.digitToInt: not a digit " ++ show c)
